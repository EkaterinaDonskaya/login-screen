//
//  Mapper.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 06.04.2022.
//

import Foundation

protocol MapperProtocol {
    func map<T: Codable>(data: Data) -> RequestResult<T>
}

final class Mapper: MapperProtocol {
    
    func map<T>(data: Data) -> RequestResult<T> where T : Codable {
        let decoder = JSONDecoder()
        do {
            let result = try decoder.decode(T.self, from: data)
            return .success(result)
            
        } catch DecodingError.dataCorrupted(let context) {
            print(DecodingError.dataCorrupted(context))
            return .failure(Error.parsing)
        } catch DecodingError.keyNotFound(let key, let context) {
            print(DecodingError.keyNotFound(key, context))
            return .failure(Error.parsing)
        } catch DecodingError.typeMismatch(let type, let context) {
            print(DecodingError.typeMismatch(type, context))
            return .failure(Error.parsing)
        } catch DecodingError.valueNotFound(let value, let context) {
            print(DecodingError.valueNotFound(value, context))
            return .failure(Error.parsing)
        } catch let error {
            print(error)
            return .failure(Error.parsing)
        }
    }
}
