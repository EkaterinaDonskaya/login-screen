//
//  NetworkRequestParams.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 06.04.2022.
//

import Foundation
import Alamofire

protocol NetworkRequestParams {
    var path: String { get }
    var method: HTTPMethod { get }
    var parameters: Parameters? { get }
    var encoding: ParameterEncoding { get }
    var baseUrl: URL { get }
    var cancelPreviousRequest: Bool { get }
    func encode<T>(item: T) -> Parameters where T : Encodable
}

extension NetworkRequestParams {
    
    var encoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    var parameters: Parameters? {
        return nil
    }
    
    var baseUrl: URL {
        return URL(string: "https://devapi.bevvy.com/api/")!
    }
    
    var cancelPreviousRequest: Bool {
        return false
    }
    
    func encode<T>(item: T) -> Parameters where T : Encodable {
        let encoder = JSONEncoder()
        let encodedData = try? encoder.encode(item)
        let parameters = try? JSONSerialization.jsonObject(with: encodedData!, options: [])
        return parameters as! Parameters
    }
}

extension URLRequestConvertible where Self:NetworkRequestParams  {
    func asURLRequest() throws -> URLRequest {
        var url = self.baseUrl
        url = url.appendingPathComponent(self.path)
    
        var request = try! URLRequest(url: url, method: self.method)
        request.cachePolicy = .reloadIgnoringLocalCacheData
        request.timeoutInterval = 30
        return try self.encoding.encode(request, with: self.parameters)
    }
}
