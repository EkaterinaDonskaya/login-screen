//
//  RequestResult.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 06.04.2022.
//

import Foundation

enum RequestResult<T> {
    case success(T)
    case failure(Error)
    
    public func dematerialize() throws -> T {
        switch self {
        case let .success(value):
            return value
        case let .failure(error):
            throw error
        }
    }
}
