//
//  BaseRouter.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 05.04.2022.
//

import UIKit

class BaseRouter {
    weak var sourceVC: UIViewController?
    
    init(sourceVC: UIViewController) {
        self.sourceVC = sourceVC
    }
}
