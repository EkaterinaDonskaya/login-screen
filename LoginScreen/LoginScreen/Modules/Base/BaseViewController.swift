//
//  BaseViewController.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 28.03.2022.
//

import UIKit
import IHProgressHUD

protocol BaseViewProtocol: AnyObject {
    func showLoader()
    func hideLoader()
    func showAlert(titleAlert: String, messageText: String, title: String)
}

class BaseViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
}

extension BaseViewController: BaseViewProtocol {
    func showLoader() {
        IHProgressHUD.set(defaultMaskType: .black)
        IHProgressHUD.show()
    }
    
    func hideLoader() {
        IHProgressHUD.dismiss()
    }
    
    func showAlert(titleAlert: String, messageText: String, title: String) {
        let cancelAction = UIAlertAction(title: title, style: .cancel, handler: { (actiion) in
            
        })
        let alert = UIAlertController(title: titleAlert, message: messageText, preferredStyle: .alert)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
}

