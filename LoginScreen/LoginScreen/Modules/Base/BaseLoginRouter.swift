//
//  BaseLoginRouter.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 05.04.2022.
//

import Foundation

protocol BaseLoginRouterProtocol: AnyObject {
    func dismiss(animated: Bool)
}

class BaseLoginRouter: BaseRouter, BaseLoginRouterProtocol {
    func dismiss(animated: Bool) {
        sourceVC?.navigationController?.popViewController(animated: animated)
    }
}
