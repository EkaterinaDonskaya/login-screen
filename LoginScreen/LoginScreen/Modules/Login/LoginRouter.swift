//
//  LoginRouter.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 05.04.2022.
//

import Foundation

protocol LoginRouterProtocol: BaseLoginRouterProtocol {
    
}

final class LoginRouter: BaseLoginRouter, LoginRouterProtocol {
    
}
