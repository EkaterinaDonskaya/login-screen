//
//  ViewController.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 28.03.2022.
//

import UIKit

protocol LoginViewControllerProtocol: BaseViewProtocol {
    
}

class LoginViewController: BaseViewController {
    
    private lazy var router: LoginRouterProtocol = LoginRouter(sourceVC: self)
    private lazy var presenter: LoginPresenterProtocol = LoginPresenter(view: self, router: router)
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var loginButton: ConfigurableButton!
    @IBOutlet weak var signUpButton: ConfigurableButton!
    
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    @IBOutlet weak var loginFieldView: UIView!
    @IBOutlet weak var loginTextField: TextFieldWithPadding!
    @IBOutlet weak var loginStateLabel: UILabel!
    
    @IBOutlet weak var passwordFieflView: UIView!
    @IBOutlet weak var passwordTextField: TextFieldWithPadding!
    @IBOutlet weak var passwordStateLabel: UILabel!
    
    @IBOutlet weak var forgotPasswordLabel: UILabel!
    
    @IBOutlet weak var authorizationButton: ConfigurableButton!
    
    @IBOutlet weak var loginWithAppleButton: ConfigurableButton!
    @IBOutlet weak var loginWithGoogleButton: ConfigurableButton!
    @IBOutlet weak var loginWithFacebookButton: ConfigurableButton!
    
    @IBOutlet weak var guestLabel: UILabel!
    
    private lazy var passwordTextFieldButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 45, height: 45))
        button.backgroundColor = UIColor.clear
        button.setImage(UIImage(named: "show"), for: .normal)
        button.addTarget(self, action: #selector(showPassword), for: .touchUpInside)
        return button
    }()
    
    private var headerButtons = [ConfigurableButton]()
    private var loginButtons = [ConfigurableButton]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        setDelegates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setDesign()
    }
    
    // MARK: - Private
    
    private func setDelegates() {
        loginTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    private func setDesign() {
        loginButton.configure(backgroundNormal: .systemBackground, backgroundSelected: .systemBackground, titleColorNormal: Color.headerTitleColor, titleColorSelected: Color.headerTitleSelectedColor, font: UIFont(name: "OpenSans-Bold", size: 16) ?? UIFont.systemFont(ofSize: 16), title: "LOG IN", for: .normal)
        signUpButton.configure(backgroundNormal: .systemBackground, backgroundSelected: .systemBackground, titleColorNormal: Color.headerTitleColor, titleColorSelected: Color.headerTitleSelectedColor, font: UIFont(name: "OpenSans-Bold", size: 16) ?? UIFont.systemFont(ofSize: 16), title: "SIGN UP", for: .normal)
        
        headerButtons = [loginButton, signUpButton]
        headerButtons.forEach { $0.isSelect = false }
        loginButton.isSelect = true
        
        authorizationButton.configure(backgroundNormal: Color.authorizationButtonColor, backgroundSelected: Color.authorizationButtonColor, titleColorNormal: .white, titleColorSelected: .white, font: UIFont(name: "OpenSans-Bold", size: 18) ?? UIFont.systemFont(ofSize: 18), title: "Log in", cornerRadius: authorizationButton.frame.height / 2, for: .normal)
        
        loginWithAppleButton.configure(backgroundNormal: Color.loginWithSocialButtonColor, backgroundSelected: Color.loginWithSocialButtonColor, titleColorNormal: Color.loginWithSocialTitleColor, titleColorSelected: Color.loginWithSocialTitleColor, font: UIFont(name: "OpenSans-Regular", size: 18) ?? UIFont.systemFont(ofSize: 18), title: "Apple", cornerRadius: loginWithAppleButton.frame.height / 2, borderWith: 1, borderColor: Color.loginWithSocialBorderColor, selectedBorderColor: Color.loginWithSocialSelectedBorderColor, asset: UIImage(named: "apple"), imagePadding: 12, for: .normal)
        
        loginWithGoogleButton.configure(backgroundNormal: Color.loginWithSocialButtonColor, backgroundSelected: Color.loginWithSocialButtonColor, titleColorNormal: Color.loginWithSocialTitleColor, titleColorSelected: Color.loginWithSocialTitleColor, font: UIFont(name: "OpenSans-Regular", size: 18) ?? UIFont.systemFont(ofSize: 18), title: "Google", cornerRadius: loginWithAppleButton.frame.height / 2, borderWith: 1, borderColor: Color.loginWithSocialBorderColor, selectedBorderColor: Color.loginWithSocialSelectedBorderColor, asset: UIImage(named: "google"), imagePadding: 12, for: .normal)
        
        loginWithFacebookButton.configure(backgroundNormal: Color.loginWithSocialButtonColor, backgroundSelected: Color.loginWithSocialButtonColor, titleColorNormal: Color.loginWithSocialTitleColor, titleColorSelected: Color.loginWithSocialTitleColor, font: UIFont(name: "OpenSans-Regular", size: 18) ?? UIFont.systemFont(ofSize: 18), title: "Facebook", cornerRadius: loginWithAppleButton.frame.height / 2, borderWith: 1, borderColor: Color.loginWithSocialBorderColor, selectedBorderColor: Color.loginWithSocialSelectedBorderColor, asset: UIImage(named: "facebook"), imagePadding: 12, for: .normal)
        
        loginButtons = [loginWithAppleButton, loginWithGoogleButton, loginWithFacebookButton]
        loginButtons.forEach { $0.isSelect = false }
        
        headerView.addShadow(color:.gray, offSet: CGSize(width: 0, height: 14), shadowRadius: 7, shadowOpacity: 0.2)
        segmentControl.selectedSegmentIndex = 0
        segmentControl.selectedSegmentTintColor = Color.headerTitleSelectedColor
        
        loginTextField.layer.borderColor = Color.textfieldBorderColor.cgColor
        loginTextField.layer.borderWidth = 1.0
        loginTextField.layer.cornerRadius = 4
        loginTextField.layer.masksToBounds = true
        loginTextField.backgroundColor = .systemBackground
        
        passwordTextField.layer.borderColor = Color.textfieldBorderColor.cgColor
        passwordTextField.layer.borderWidth = 1.0
        passwordTextField.layer.cornerRadius = 4
        passwordTextField.layer.masksToBounds = true
        passwordTextField.backgroundColor = .systemBackground
        
        passwordTextFieldButton.widthAnchor.set(to: 50.0)
        passwordTextField.rightView = passwordTextFieldButton
        passwordTextField.rightViewMode = .always
        passwordTextField.isSecureTextEntry = true
    }
    
    private func configureUI() {
        
        fixBackgroundSegmentControl(segmentControl)
        
        loginStateLabel.text = " "
        passwordStateLabel.text = " "
        
        forgotPasswordLabel.isUserInteractionEnabled = true
        let tapGestureRecognezer = UITapGestureRecognizer(target: self, action: #selector(forgotPassword(_ :)))
        forgotPasswordLabel.addGestureRecognizer(tapGestureRecognezer)
        
        guestLabel.isUserInteractionEnabled = true
        let guestTapGestureRecognezer = UITapGestureRecognizer(target: self, action: #selector(guestLabelAction(_ :)))
        guestLabel.addGestureRecognizer(guestTapGestureRecognezer)
    }
    
    private func fixBackgroundSegmentControl( _ segmentControl: UISegmentedControl){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            for i in 0...(segmentControl.numberOfSegments-1)  {
                let backgroundSegmentView = segmentControl.subviews[i]
                backgroundSegmentView.isHidden = true
            }
        }
    }
    
    private func changeStateOfHeaderButton(_ button: ConfigurableButton) {
        if !button.isSelect {
            headerButtons.forEach { $0.isSelect = false }
            button.isSelect = true
        }
    }
    
    private func changeStateOfLoginButton(_ button: ConfigurableButton) {
        if !button.isSelect {
            loginButtons.forEach { $0.isSelect = false }
            button.isSelect = true
        }
    }
    
    // MARK: - Actions
    
    @IBAction func loginButtonAction(_ sender: ConfigurableButton) {
        changeStateOfHeaderButton(sender)
        segmentControl.selectedSegmentIndex = 0
    }
    
    @IBAction func signUpButtonAction(_ sender: ConfigurableButton) {
        changeStateOfHeaderButton(sender)
        segmentControl.selectedSegmentIndex = 1
    }
    
    @IBAction func authorizationButtonAction(_ sender: ConfigurableButton) {
        guard let email = loginTextField.text, !email.isEmpty,
              let password = passwordTextField.text, !password.isEmpty else {
            showAlert(titleAlert: "Error", messageText: "Fill email and password fields", title: "OK")
            return }
        
        if presenter.isValidEmail(email: email) {
            presenter.signIn(email: email, password: password)
        } else {
            showAlert(titleAlert: "Error", messageText: "Email is not valid", title: "OK")
        }
    }
    
    @IBAction func loginWithAppleButtonAction(_ sender: ConfigurableButton) {
        changeStateOfLoginButton(sender)
    }
    
    @IBAction func loginWithGoogleButtonAction(_ sender: ConfigurableButton) {
        changeStateOfLoginButton(sender)
    }
    
    @IBAction func loginWithFacebookButtonAction(_ sender: ConfigurableButton) {
        changeStateOfLoginButton(sender)
    }
    
    @objc func forgotPassword(_ sender: UILabel) {
        debugPrint("forgot Password")
    }
    
    @objc func guestLabelAction(_ sender: UILabel) {
        debugPrint("I am guest")
    }
    
    @objc func showPassword() {
        passwordTextFieldButton.isSelected.toggle()
        passwordTextField.isSecureTextEntry = false
        
        if !passwordTextFieldButton.isSelected {
            passwordTextField.isSecureTextEntry = true
        }
    }
}

// MARK: - LoginViewControllerProtocol
extension LoginViewController: LoginViewControllerProtocol {}

// MARK: - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.layer.borderColor = Color.textfieldSelectedBorderColor.cgColor
        textField.layer.borderWidth = 2.0
        textField.layer.cornerRadius = 4
        textField.layer.masksToBounds = true
        
        if textField == loginTextField {
            loginStateLabel.text = "Focused"
        } else if textField == passwordTextField {
            passwordStateLabel.text = "Focused"
        } else {
            loginStateLabel.text = " "
            passwordStateLabel.text = " "
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.layer.borderColor = Color.textfieldBorderColor.cgColor
        textField.layer.borderWidth = 1.0
        textField.layer.cornerRadius = 4
        textField.layer.masksToBounds = true
        
        if textField == loginTextField {
            if let text = textField.text, !text.isEmpty {
                loginStateLabel.text = "Activated"
            } else {
                loginStateLabel.text = " "
            }
        } else if textField == passwordTextField {
            if let text = textField.text, !text.isEmpty {
                passwordStateLabel.text = "Activated"
            } else {
                passwordStateLabel.text = " "
            }
        } else {
            loginStateLabel.text = " "
            passwordStateLabel.text = " "
        }
    }
}
