//
//  LoginPresenter.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 05.04.2022.
//

import Foundation

protocol LoginPresenterProtocol: AnyObject {
    func isValidEmail(email: String) -> Bool
    func signIn(email: String, password: String)
}

final class LoginPresenter: LoginPresenterProtocol {
    
    private weak var view: LoginViewControllerProtocol?
    private let router: LoginRouterProtocol?
    private let networkService: LoginNetworkServiceProtocol?
    private let tokenService: TokenServiceProtocol?
    
    init(view: LoginViewControllerProtocol,
         router: LoginRouterProtocol,
         networkService: LoginNetworkServiceProtocol = LoginNetworkService.shared,
         tokenService: TokenServiceProtocol = TokenService.shared) {
        self.view = view
        self.router = router
        self.networkService = networkService
        self.tokenService = tokenService
    }
    
    func isValidEmail(email: String) -> Bool {
        let emailSymbols = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailSymbols)
        return emailPredicate.evaluate(with: email)
    }
    
    func signIn(email: String, password: String) {
        view?.showLoader()
        networkService?.signIn(email: email, password: password, completionHandler: { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let loginResponse):
                self.view?.hideLoader()
                if let message = loginResponse.message, let token = loginResponse.token {
                    self.view?.showAlert(titleAlert: "Success", messageText: message, title: "OK")
                    self.tokenService?.saveAuthToken(token)
                }
            case .failure(let error):
                self.view?.hideLoader()
                if let message = error.errorMessage {
                    self.view?.showAlert(titleAlert: "Error", messageText: message, title: "OK")
                } else {
                    debugPrint(error.localizedDescription)
                }
            }
        })
    }
}
