//
//  TokenService.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 06.04.2022.
//

import Foundation

protocol TokenServiceProtocol {
    var authToken: String? { get }
    func saveAuthToken(_ token: String)
    func clearAuthToken()
}

final class TokenService: TokenServiceProtocol {
    static let shared = TokenService()
    
    private let defaults = UserDefaults.standard
    private let authTokenKey = "authToken"

    private init() {}
    
    // MARK: - UserDefManagerInterface
        
    var authToken: String? {
        UserDefaults.standard.string(forKey: authTokenKey)
    }
    
    func saveAuthToken(_ token: String) {
        UserDefaults.standard.set(token, forKey: authTokenKey)
    }
    
    func clearAuthToken() {
        UserDefaults.standard.removeObject(forKey: authTokenKey)
    }
}
