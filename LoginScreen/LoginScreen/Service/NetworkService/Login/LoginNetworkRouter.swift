//
//  LoginNetworkRouter.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 06.04.2022.
//

import Foundation
import Alamofire

enum LoginNetworkRouter: URLRequestConvertible {
    case signIn(email: String, password: String)
}

extension LoginNetworkRouter: NetworkRequestParams {
    
    var path: String {
        switch self {
        case .signIn:
            return "signin"
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .signIn(let email, let password):
            return ["email": email,
                    "password": password]
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .signIn: return .post
        }
    }
}
