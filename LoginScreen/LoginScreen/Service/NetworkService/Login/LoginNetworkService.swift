//
//  LoginNetworkService.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 06.04.2022.
//

import Foundation
import Alamofire

protocol LoginNetworkServiceProtocol {
    func signIn(email: String, password: String, completionHandler: @escaping (RequestResult<LoginResponse>) -> Void)
}

final class LoginNetworkService: LoginNetworkServiceProtocol {
    
    static let shared = LoginNetworkService()
    
    private let httpClient: HttpClientProtocol
    
    init(httpClient: HttpClientProtocol = HttpClient()) {
        self.httpClient = httpClient
    }
    
    func signIn(email: String, password: String, completionHandler: @escaping (RequestResult<LoginResponse>) -> Void) {
        let request = LoginNetworkRouter.signIn(email: email, password: password)
        httpClient.load(request: request, completionHandler: completionHandler)
    }
}
