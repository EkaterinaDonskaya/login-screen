//
//  LoginResponse.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 06.04.2022.
//

import Foundation

struct LoginResponse: Codable {
    let token, message: String?
    
    enum CodingKeys: String, CodingKey {
        case token = "data"
        case message
    }
}
