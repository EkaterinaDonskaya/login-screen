//
//  UIView + Extension.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 28.03.2022.
//

import UIKit

extension UIView {
    func addShadow(color: UIColor = UIColor.black, offSet: CGSize = CGSize(width: 0, height: 0), shadowRadius: CGFloat = 10, cornerRadius: CGFloat = 0, shadowOpacity: Float = 1.0, maskedCorners: CACornerMask? = nil) {
        layer.cornerRadius = cornerRadius
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offSet
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
        if let maskedCorners = maskedCorners {
            layer.maskedCorners = maskedCorners
        }
    }
}

