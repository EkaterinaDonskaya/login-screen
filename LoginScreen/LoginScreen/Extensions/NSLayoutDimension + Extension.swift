//
//  NSLayoutDimension + Extension.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 28.03.2022.
//

import UIKit

extension NSLayoutDimension {

@discardableResult
func set(to constant: CGFloat, priority: UILayoutPriority = .required) -> NSLayoutConstraint {
        let cons = constraint(equalToConstant: constant)
        cons.priority = priority
        cons.isActive = true
        return cons
    }
}

