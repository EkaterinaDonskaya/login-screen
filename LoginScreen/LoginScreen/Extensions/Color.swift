//
//  Color.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 05.04.2022.
//

import UIKit

final class Color {
    
    static let headerTitleSelectedColor = UIColor(hex: 0x333333)
    static let headerTitleColor = UIColor(hex: 0x8A8A8A)
    
    static let textfieldBorderColor: UIColor = {
        return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark {
                return UIColor(hex: 0xE6E6E6)
            } else {
                return UIColor(red: 0, green: 0, blue: 0, alpha: 0.12)
            }
        }
    }()
    
    static let textfieldSelectedBorderColor: UIColor = {
        return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark {
                return UIColor.white
            } else {
                return UIColor(hex: 0x333333)
            }
        }
    }()
    
    static let authorizationButtonColor = UIColor(hex: 0x333333)
    
    static let loginWithSocialTitleColor = UIColor(hex: 0x121212)
    static let loginWithSocialBorderColor = UIColor(hex: 0xE6E6E6)
    static let loginWithSocialSelectedBorderColor: UIColor = {
        return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark {
                return UIColor.white
            } else {
                return UIColor.black
            }
        }
    }()
    
    
    static let loginWithSocialButtonColor: UIColor = {
        return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
            if UITraitCollection.userInterfaceStyle == .dark {
                return UIColor.systemGray
            } else {
                return UIColor.white
            }
        }
    }()
    
}

extension UIColor {
    convenience init(hex: Int, alpha: CGFloat = 1) {
        let components = (
            R: CGFloat((hex >> 16) & 0xff) / 255,
            G: CGFloat((hex >> 08) & 0xff) / 255,
            B: CGFloat((hex >> 00) & 0xff) / 255
        )
        
        self.init(red: components.R, green: components.G, blue: components.B, alpha: alpha)
    }
}
