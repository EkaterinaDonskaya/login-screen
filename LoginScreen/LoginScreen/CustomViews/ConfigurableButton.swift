//
//  ConfigurableButton.swift
//  LoginScreen
//
//  Created by Ekaterina Donskaya on 05.04.2022.
//

import UIKit

class ConfigurableButton: UIButton {
    
    private var cornerRadius: CGFloat = 0
    private var borderWith: CGFloat = 0
    private var borderColor: UIColor?
    private var selectedBorderColor: UIColor?
    private var maskedCorners: CACornerMask?
    
    private var titleColorNormal = UIColor.clear
    private var titleColorSelected = UIColor.clear
    private var backgroundNormal = UIColor.clear
    private var backgroundSelected = UIColor.clear
    
    private var title: String?
    private var font: UIFont?
    
    // MARK: - Public
    
    func configure(backgroundNormal: UIColor, backgroundSelected: UIColor, titleColorNormal: UIColor, titleColorSelected: UIColor, font: UIFont, title: String, cornerRadius: CGFloat? = nil, maskedCorners: CACornerMask? = nil, borderWith: CGFloat? = nil, borderColor: UIColor? = nil, selectedBorderColor: UIColor? = nil, asset: UIImage? = nil, imagePadding: CGFloat = 0, for state: UIControl.State) {
        var configuration = UIButton.Configuration.plain()
        configuration.image = asset
        configuration.imagePadding = imagePadding
        configuration.attributedTitle = AttributedString(title, attributes: AttributeContainer([NSAttributedString.Key.foregroundColor: titleColorNormal, NSAttributedString.Key.font: font]))
        
        self.configuration = configuration
        
        
        backgroundColor = backgroundNormal
        self.titleColorNormal = titleColorNormal
        self.titleColorSelected = titleColorSelected
        self.backgroundNormal = backgroundNormal
        self.backgroundSelected = backgroundSelected
        self.cornerRadius = cornerRadius ?? 0
        self.borderWith = borderWith ?? 0
        self.borderColor = borderColor ?? nil
        self.selectedBorderColor = selectedBorderColor ?? nil
        self.maskedCorners = maskedCorners
        self.title = title
        self.font = font
        
        titleLabel?.adjustsFontSizeToFitWidth = true
        titleLabel?.minimumScaleFactor = 0.5
    }
    
    // MARK: - Override
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
    }
    
    var isSelect: Bool = false {
        didSet {
            backgroundColor = isSelect ? backgroundSelected : backgroundNormal
            layer.borderColor = isSelect ? selectedBorderColor?.cgColor : borderColor?.cgColor
            let color = isSelect ? titleColorSelected : titleColorNormal
            self.configuration?.attributedTitle = AttributedString(title ?? "", attributes: AttributeContainer([NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font: font ?? UIFont.systemFont(ofSize: 16)]))
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWith
        layer.borderColor = isSelect ? selectedBorderColor?.cgColor : borderColor?.cgColor
        layer.maskedCorners = maskedCorners ?? [.layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
}

